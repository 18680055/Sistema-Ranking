const express =require('express');

const dotenv= require('dotenv');
const cookieParser= require('cookie-parser');

const app= express();
const {body, validatorResult} =require('express-validator');
//seteamos el motor de plantillas
app.set('view engine', 'ejs');
//seteamos la carpeta public para archivos estaticos
app.use(express.static('public'));
//para procesar datos enviados desde forms
app.use(express.urlencoded({extended:true}));
app.use(express.json());
//seteamos las variables de entorno
dotenv.config({path: './env/.env'});
//para trabajar con cookies
app.use(cookieParser());
// llamar a la ruta
app.use('/', require('./routes/routes'));
 
//eliminar el cache
app.use(function(req, res, next) {
    if (!req.idusuarios)
        res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate, max-age=300');
    next();
});
app.listen(3000, ()=>{
    console.log('SERVER CORRIENDO EN http://localhost:3000')
});
