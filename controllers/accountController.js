
const conexion = require('../db/db');
const crypto = require('crypto-js');
const jwt = require('jsonwebtoken');
const { promisify } = require('util');

function obtenerContrasena(usuario) {
    const desencriptar = crypto.AES.decrypt(usuario.contra, process.env.FIRMA);
    return desencriptar.toString(crypto.enc.Utf8);
}

//metodo sign in
exports.login = async (req, res) => {
    try {
        const idusuarios = req.body.idusuarios
        const contra = req.body.contra
        conexion.query('SELECT * FROM usuarios WHERE id_usuario = ?', [idusuarios], async (error, results) => {
            if (results.length > 0) {
                let usuario = results[0];
                if(contra == obtenerContrasena(usuario)){
                    if (usuario.estatus != 'Activo') {
                        res.render('account/login', {
                            alert: true,
                            alertTitle: "Advertencia",
                            alertMessage: "Cuenta Suspendida",
                            alertIcon: "warning",
                            showConfirmButton: false,
                            timer: 800,
                            ruta: 'login'
                        })
                    } else {
                        const token = jwt.sign({ idusuarios: idusuarios }, process.env.JWT_SECRETO, { expiresIn: process.env.JWT_TIEMPO })
                        const cookiesOption = {
                            expiresIn: new Date(Date.now() + process.env.JWT_COOKIES * 24 * 60 * 60 * 1000),
                            httpOnly: true
                        }
                        res.cookie('jwt', token, cookiesOption)
                        switch (usuario.rol) {
                            case "Administrador":
                                res.render('account/login', {
                                    alert: true,
                                    alertTitle: "¡Bienvenido!",
                                    alertMessage: usuario.id_usuario,
                                    alertIcon: "success",
                                    showConfirmButton: false,
                                    timer: 800,
                                    ruta: ''
                                })
                                break;
                            case "Profesor":
                                res.render('account/login', {
                                    alert: true,
                                    alertTitle: "¡Bienvenido!",
                                    alertMessage: usuario.id_usuario,
                                    alertIcon: "success",
                                    showConfirmButton: false,
                                    timer: 800,
                                    ruta: ''
                                })
                                break;
                            case "Alumno":
                                res.render('account/login', {
                                    alert: true,
                                    alertTitle: "¡Bienvenido!",
                                    alertMessage: usuario.id_usuario,
                                    alertIcon: "success",
                                    showConfirmButton: false,
                                    timer: 800,
                                    ruta: 'alumno/'+usuario.id_usuario
                                })
                                break;
                            case "Empresa":
                                res.render('account/login', {
                                    alert: true,
                                    alertTitle: "¡Bienvenido!",
                                    alertMessage: usuario.id_usuario,
                                    alertIcon: "success",
                                    showConfirmButton: false,
                                    timer: 800,
                                    ruta: 'empresa/' + usuario.id_usuario
                                })
                                break;
                            default:
                                res.render('account/login', {
                                    alert: true,
                                    alertTitle: "¡Oh oh!",
                                    alertMessage: "Usuario sin rol. Contacte al administrador",
                                    alertIcon: "warning",
                                    showConfirmButton: false,
                                    timer: 800,
                                    ruta: 'login'
                                })
                                break;
                        }
                    }
                }
            }
            res.render('account/login', {
                alert: true,
                alertTitle: "Error",
                alertMessage: "Nombre de usuario y/o contraseña incorrectos",
                alertIcon: "error",
                showConfirmButton: false,
                timer: 800,
                ruta: 'login'
            })
        });
    } catch (error) {
        console.log(error);
    }
}
//metodo de autentificacion
exports.isAuthenticated = async (req, res, next) => {

    if (req.cookies.jwt) {
        try {

            const decodificada = await promisify(jwt.verify)(req.cookies.jwt, process.env.JWT_SECRETO)
            conexion.query('SELECT*FROM usuarios WHERE id_usuario = ?', [decodificada.idusuarios], (error, results) => {
                if (!results) { return next() }
                req.usuario = results[0]
                // console.log("Rol:",req.usuario.rol)
                return next()
            })

            //console.log("idusuarios: ",[decodificada.idusuarios])
        } catch (error) {
            console.log(error)
            return next()
        }
    } else {
        res.redirect('/login')
    }
}
//metodo de cierre de sesion
exports.logout = (req, res) => {
    res.clearCookie('jwt')
    return res.redirect('/')
}