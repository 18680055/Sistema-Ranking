const conexion = require('../db/db');
const crypto = require('crypto-js');

/*----------------------------------------------------------------- */
//metodo listar administradores
exports.listaAdministradores = async (req, res, next) => {
    conexion.query('SELECT usuarios.id_usuario, profesores.nombre, profesores.ap_paterno, profesores.ap_materno, profesores.correo, usuarios.rol FROM profesores INNER JOIN usuarios ON usuarios.id_usuario=profesores.id_usuario WHERE usuarios.rol="Administrador";',
        async (error, results) => {
            if (!results) { return next() }
            req.administradores = results
            return next();
        })
}
//informacion de los admins
exports.infoAdmins = async (req, res, next) => {
    try {
        const id = req.params.id
        //console.log("id: ",id)
        conexion.query('SELECT*FROM usuarios WHERE id_usuario=?', [id], async (error, results) => {
            if (!results) { return next() }
            const desencriptar = crypto.AES.decrypt(results[0].contra, "firma");
            req.contra = desencriptar.toString(crypto.enc.Utf8)
            // console.log(req.contra)
            req.adminU = results[0]
            //console.log('id usuarios',req.adminU)
            conexion.query('SELECT*FROM profesores WHERE id_usuario=? ', [id], async (error, results) => {
                if (!results) { return next() }
                req.adminA = results[0]
                // console.log("A: ",req.adminA)
                return next();
            });
        });



    } catch (error) {
        console.log(error)
    }
}
exports.actualizar_administrador = async (req, res, next) =>
{
    try
    {
        let sql = '';
        let data = [];
        const id_usuario = req.body.id_usuario;
        const rol = req.body.rol;
        const contra = req.body.contra;
        const estatus = req.body.estatus;
        const nombre = req.body.nombre;
        const ap_paterno = req.body.ap_paterno;
        const ap_materno = req.body.ap_materno;
        const cedula = req.body.cedula;
        const correo = req.body.correo;
        const grado_academico = req.body.grado_academico;
        const tipo_contrato = req.body.tipo_contrato;
        const tipo_academia = req.body.tipo_academia;
        const ContraHash = crypto.AES.encrypt(contra, process.env.FIRMA)

        sql = `UPDATE usuarios SET contra = ?, rol = ?, estatus = ? WHERE id_usuario = ?`;
        data = [ContraHash.toString(), rol, estatus, id_usuario];
        conexion.query(sql, data, async (error, results) =>
        {
            if (error)
            {
                res.render('_shared/ventana', {
                    alert: true,
                    alertTitle: "ALGO SALIO MAL!!!",
                    alertMessage: error.message,
                    alertIcon: "error",
                    showConfirmButton: false,
                    timer: 1000,
                    ruta: 'listar_administradores'
                });
            }
            sql = `UPDATE profesores SET 
                        nombre = ?, 
                        ap_paterno = ?, 
                        ap_materno = ?, 
                        cedula = ?, 
                        grado_academico = ?,
                        correo = ?,
                        tipo_contrato = ?,
                        tipo_academia = ?
                    WHERE id_usuario = ?`;
            data = [nombre, ap_paterno, ap_materno, cedula, grado_academico, correo, tipo_contrato, tipo_academia, id_usuario];
            conexion.query(sql, data, async (error, results) =>
            {
                if (error)
                {
                    console.log(error)
                }
                res.render('_shared/ventana', {
                    alert: true,
                    alertTitle: "ACTUALIZACIÓN EXITOSA!!!",
                    alertMessage: "USUARIO ACTUALIZADO",
                    alertIcon: "success",
                    showConfirmButton: false,
                    timer: 1000,
                    ruta: 'listar_administradores'
                });
            });
        });

    } catch (error)
    {
        res.render('_shared/ventana', {
            alert: true,
            alertTitle: "ALGO SALIO MAL!!!",
            alertMessage: error.message,
            alertIcon: "error",
            showConfirmButton: false,
            timer: 1000,
            ruta: 'listar_administradores'
        });
    }
}

exports.eliminar_administrador = async (req, res) =>
{
    let id = req.query.id
    let sql = `DELETE FROM profesores WHERE id_usuario = ?`
    conexion.query(sql, [id], async (error, results) =>
    {
        if (error)
        {
            res.render('_shared/ventana', {
                alert: true,
                alertTitle: "ALGO SALIO MAL!!!",
                alertMessage: error.message,
                alertIcon: "error",
                showConfirmButton: false,
                timer: 1000,
                ruta: 'listar_administradores'
            });
        }
        let sql = `DELETE FROM usuarios WHERE id_usuario = ?`
        conexion.query(sql, [id], async (error, results) =>
        {
            if (error)
            {
                res.render('_shared/ventana', {
                    alert: true,
                    alertTitle: "ALGO SALIO MAL!!!",
                    alertMessage: error.message,
                    alertIcon: "error",
                    showConfirmButton: false,
                    timer: 1000,
                    ruta: 'listar_administradores'
                });
            }
            res.render('_shared/ventana', {
                alert: true,
                alertTitle: "ELIMINACION EXITOSA!!!",
                alertMessage: "administrador ELIMINADO",
                alertIcon: "success",
                showConfirmButton: false,
                timer: 1000,
                ruta: 'listar_administradores'
            });
        })
    })
}