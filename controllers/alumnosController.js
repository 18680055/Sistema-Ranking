const conexion = require('../db/db');
const crypto = require('crypto-js');

//metodo registrar Alumnos
exports.registrar_alumnos = async (req, res, next) => {
    try {
        const id_usuario = req.body.id_usuario
        const rol = req.body.rol
        const contra = req.body.contra
        const estatus = req.body.estatus

        const nombre = req.body.nombre
        const ap_paterno = req.body.ap_paterno
        const ap_materno = req.body.ap_materno
        const direccion = req.body.direccion

        const correo = req.body.Correo
        const fecha_nac = req.body.fecha_nac
        const fecha_ingreso = req.body.fecha_ingreso
        const id_carrera = req.body.id_carrera
        const ContraHash = crypto.AES.encrypt(contra, "firma",);
        //let ContraHash=await bycryptjs.hash(contra, 8);
        conexion.query('SELECT*FROM usuarios WHERE id_usuario= ?', [id_usuario], async (error, results) => {
            if (error) { console.log(error) }
            if (results.length == 1) {
                res.render('_shared/ventana', {
                    alert: true,
                    alertTitle: "Advertencia",
                    alertMessage: "USUARIO YA EXISTENTE!!!",
                    alertIcon: "error",
                    showConfirmButton: false,
                    timer: 1000,
                    ruta: 'registrar_alumnos'
                });

            } else {
                conexion.query('INSERT INTO usuarios SET ?', { id_usuario: id_usuario, rol: rol, contra: ContraHash, rol: rol, estatus: estatus },
                    (error, result) => {
                        if (error) { console.log(error) }
                        conexion.query('INSERT INTO alumnos SET ?',
                            { id_usuario: id_usuario, nombre: nombre, ap_paterno: ap_paterno, ap_materno: ap_materno, fecha_nac: fecha_nac, direccion: direccion, correo: correo, fecha_ingreso: fecha_ingreso, id_carrera: id_carrera }, (error, result) => {
                                if (error) { console.log(error) }
                            });
                        //res.redirect('/listar_alumnos');
                        res.render('_shared/ventana', {
                            alert: true,
                            alertTitle: "REGISTRO EXITOSO!!!",
                            alertMessage: "USUARIO REGISTRADO",
                            alertIcon: "success",
                            showConfirmButton: false,
                            timer: 1000,
                            ruta: 'listar_alumnos'
                        });
                    }
                );
            }
        })

    } catch (error) {
        console.log(error);
    }
}
//metodo listar alumnos
exports.listaAlumnos = async (req, res, next) => {
    conexion.query(
        'SELECT alumnos.id_usuario, alumnos.nombre,alumnos.ap_paterno, alumnos.ap_materno, alumnos.direccion, alumnos.correo, carreras.nombre AS carrera FROM alumnos INNER JOIN carreras ON alumnos.id_carrera = carreras.id_carrera; ',
        async (error, results) => {
            if (!results) { return next() }
            req.alumnos = results
            //console.log("Alumnos",req.alumnos)
            return next();
        })
}
//metodo visualizar informacion alumno a actualizar
exports.infoAlumno = async (req, res, next) => {
    try {
        const actualizar = req.params.id
        console.log("id", actualizar)
        conexion.query('SELECT*FROM usuarios WHERE id_usuario=?', [actualizar], async (error, results) => {
            if (!results) { return next() }
            const desencriptar = crypto.AES.decrypt(results[0].contra, "firma");
            req.contra = desencriptar.toString(crypto.enc.Utf8)
            console.log(req.contra)
            req.alumnoU = results[0]
            conexion.query('SELECT alumnos.id_usuario, alumnos.nombre, alumnos.ap_paterno, alumnos.ap_materno,DATE_FORMAT(alumnos.fecha_nac,"%Y-%m-%d ") AS fecha_nac, alumnos.direccion, alumnos.correo, DATE_FORMAT(alumnos.fecha_ingreso,"%Y-%m-%d ") AS fecha_ingreso, alumnos.id_carrera, carreras.nombre AS carrera FROM alumnos INNER JOIN carreras ON alumnos.id_carrera=carreras.id_carrera WHERE id_usuario=?;',
                [actualizar], async (error, results) => {
                    if (!results) { return next() }
                    req.alumnoA = results[0]
                    console.log("usuarios ", req.alumnoU, "Alumno", req.alumnoA, " fehca", req.alumnoA.fecha)
                    return next();
                });
        });


    } catch (error) {
        console.log(error)
    }
}
//metodo actualizar informacion
exports.actualizar_alumnos = async (req, res, next) => {
    try {
        const id_usuario = req.body.id_usuario
        const id_usuarioA = req.body.id_usuarioA
        console.log("id de la info", id_usuario, " ", id_usuarioA)
        const rol = req.body.rol
        const contra = req.body.contra
        const estatus = req.body.estatus

        const nombre = req.body.nombre
        const ap_paterno = req.body.ap_paterno
        const ap_materno = req.body.ap_materno
        const direccion = req.body.direccion

        const correo = req.body.Correo
        const fecha_nac = req.body.fecha_nac
        const fecha_ingreso = req.body.fecha_ingreso
        const id_carrera = req.body.id_carrera
        const ContraHash = crypto.AES.encrypt(contra, "firma",);
        //let ContraHash=await bycryptjs.hash(contra, 8);
        //manera en la que se pone es en la que lo interpreta
        conexion.query('UPDATE usuarios SET ? WHERE id_usuario = ?;', [{ id_usuario: id_usuario, contra: ContraHash, rol: rol, estatus: estatus }, id_usuarioA], async (error, results) => {
            if (error) { console.log(error) }
            conexion.query('UPDATE alumnos SET ? WHERE id_usuario=?;',
                [{ nombre: nombre, ap_paterno: ap_paterno, ap_materno: ap_materno, fecha_nac: fecha_nac, direccion: direccion, correo: correo, fecha_ingreso: fecha_ingreso, id_carrera: id_carrera }, id_usuario], async (error, results) => {
                    if (error) { console.log(error) }

                });
        });
        res.render('_shared/ventana', {
            alert: true,
            alertTitle: "ACTUALIZACION EXITOSA!!!",
            alertMessage: "USUARIO ACTUALIZADO",
            alertIcon: "success",
            showConfirmButton: false,
            timer: 1000,
            ruta: 'listar_alumnos'
        });

    } catch (error) {
        console.log(error)
    }
}