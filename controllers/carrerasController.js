const conexion = require('../db/db');
const crypto = require('crypto-js');

exports.registrar_carrera = async (req, res, next) => {
    let sql = 'INSERT INTO carreras SET ?'
    let data = req.body
    conexion.query(sql, data, async (error, results) => {
        if (error) {
            console.log(error);
            res.render('_shared/ventana', {
                alert: true,
                alertTitle: "Algo salió mal 😔",
                alertMessage: "La carrera no pudo ser registrada, inténtelo nuevamente.",
                alertIcon: "danger",
                showConfirmButton: false,
                timer: 1200,
                ruta: 'registrar_carrera'
            });
        }
        res.render('_shared/ventana', {
            alert: true,
            alertTitle: "¡Éxito! 💯",
            alertMessage: "La carrera se registró correctamente.",
            alertIcon: "success",
            showConfirmButton: false,
            timer: 1000,
            ruta: 'carreras'
        });
    })
}
exports.obtenerCarreras = async (req, res, next) => {
    conexion.query('SELECT id_carrera, nombre FROM carreras',async (error, results) => {
        if (!results) { 
            req.carreras = new Array() 
            return next() 
        }
        req.carreras = results
        return next()
    })
}
exports.obtenerCarrera = async (req, res, next) => {
    let id_carrera = req.query.id;
    conexion.query('SELECT * FROM carreras WHERE id_carrera = ?', [id_carrera],async (error, results) => {
        if (!results) { 
            req.carrera = undefined
            return next() 
        }
        req.carrera = results[0]
        return next()
    })
}
exports.actualizar_carrera = async (req, res) => {
    let sql = `UPDATE carreras SET ? WHERE id_carrera = ?`
    let id_carrera = req.body.id_carrera
    let data = {
        nombre: req.body.nombre,
        coordinador: req.body.coordinador,
        correo: req.body.correo
    }
    conexion.query(sql, [data, id_carrera],async (error, results) => {
        if (error) { 
            console.log(error);
            res.render('_shared/ventana', {
                alert: true,
                alertTitle: "Algo salió mal 😔",
                alertMessage: "La carrera no pudo ser actualizada, inténtelo nuevamente.",
                alertIcon: "danger",
                showConfirmButton: false,
                timer: 1200,
                ruta: 'actualizar_carrera?id='+id_carrera
            });
        }
        res.render('_shared/ventana', {
            alert: true,
            alertTitle: "¡Éxito! 💯",
            alertMessage: "La carrera se actualizó correctamente.",
            alertIcon: "success",
            showConfirmButton: false,
            timer: 1000,
            ruta: 'actualizar_carrera?id='+id_carrera
        });
    })
}
exports.eliminar_carrera = async (req, res) => {
    let sql = `DELETE FROM carreras WHERE id_carrera = ?`
    let id_carrera = req.body.id_carrera
    conexion.query(sql, [id_carrera],async (error, results) => {
        if (error) { 
            console.log(error)
            res.json({ result: false })
        }
        res.json({ result: true })
    })
}