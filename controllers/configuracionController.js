const conexion = require('../db/db');
const crypto = require('crypto-js');

exports.obtenerConfiguracion = async (req, res, next) => {
    let sql = `SELECT * FROM configuraciones`;
    conexion.query(sql, async (error, results) => {
        if(!results){
            return next();
        }
        req.configuracion = results[0];
        return next();
    });
}
exports.guardarConfiguracion = async (req, res) => {
    let data = req.body;
    let sql = `UPDATE configuraciones SET ?`
    conexion.query(sql, [data],async (error, results) => {
        if (error) { 
            console.log(error);
            res.render('_shared/ventana', {
                alert: true,
                alertTitle: "Algo salió mal 😔",
                alertMessage: "La configuraciòn no pudo ser guardada, inténtelo nuevamente.",
                alertIcon: "danger",
                showConfirmButton: false,
                timer: 1200,
                ruta: 'config'
            });
        }
        res.render('_shared/ventana', {
            alert: true,
            alertTitle: "¡Éxito! 💯",
            alertMessage: "La configuración se guardó correctamente.",
            alertIcon: "success",
            showConfirmButton: false,
            timer: 1000,
            ruta: 'config'
        });
    })
}