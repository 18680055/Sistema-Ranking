const conexion = require('../db/db');
const crypto = require('crypto-js');

exports.registrar_curso = async (req, res, next) => {
    let sql = 'INSERT INTO cursos SET ?'
    let data = req.body
    conexion.query(sql, data, async (error, results) => {
        if (error) {
            console.log(error.sql);
            console.log(error);
            res.render('_shared/ventana', {
                alert: true,
                alertTitle: "Algo salió mal 😔",
                alertMessage: "El curso no pudo ser registrado, inténtelo nuevamente.",
                alertIcon: "danger",
                showConfirmButton: false,
                timer: 1200,
                ruta: `cursos`
            });
        }
        res.render('_shared/ventana', {
            alert: true,
            alertTitle: "¡Éxito! 💯",
            alertMessage: "El curso se registró correctamente.",
            alertIcon: "success",
            showConfirmButton: false,
            timer: 1000,
            ruta: 'cursos'
        });
    })
}
exports.obtenerCursos = async (req, res, next) => {
    let sql = `SELECT c.id_curso, m.nombre_materia as materia, g.nombre as grupo, g.carrera as carrera, prof.id_usuario as profesor, p.descripcion as periodo FROM cursos as c
                INNER JOIN materias as m ON c.materia = m.id_materia
                INNER JOIN grupos as g ON c.grupo = g.id_grupo
                INNER JOIN profesores as prof ON c.profesor = prof.num_profesor
                INNER JOIN periodos as p ON c.periodo = p.id_periodo`
    conexion.query(sql,async (error, results) => {
        if (!results) { 
            req.cursos = new Array() 
            return next() 
        }
        req.cursos = results
        return next()
    })
}
exports.obtenerCursosPeriodoCarrera = async (req, res, next) => {
    let periodo = parseInt(req.body.periodo);
    let carrera = parseInt(req.body.carrera);
    let sql = `SELECT c.id_curso, m.nombre_materia as materia, g.nombre as grupo, g.carrera as carrera, CONCAT(prof.ap_paterno," ",prof.ap_materno," ",prof.nombre) as profesor, p.descripcion as periodo FROM cursos as c
                INNER JOIN materias as m ON c.materia = m.id_materia
                INNER JOIN grupos as g ON c.grupo = g.id_grupo
                INNER JOIN profesores as prof ON c.profesor = prof.num_profesor
                INNER JOIN periodos as p ON c.periodo = p.id_periodo
                WHERE p.id_periodo = ? AND m.id_carrera = ?`
    conexion.query(sql, [periodo, carrera],async (error, results) => {
        if (!results) { 
            req.cursos = new Array() 
            return next() 
        }
        req.cursos = results
        return next()
    })
}
exports.obtenerCurso = async (req, res, next) => {
    let sql = `SELECT * FROM cursos as c
                WHERE c.id_curso = ?`;
    let id_curso = req.query.id;
    conexion.query(sql, [id_curso],async (error, results) => {
        if (!results) { 
            req.curso = undefined
            return next() 
        }
        req.curso = results[0]
        return next()
    })
}
exports.actualizar_curso = async (req, res) => {
    let sql = `UPDATE cursos SET ? WHERE id_curso = ?`
    let id_curso = req.body.id_curso
    let data = {
        materia: req.body.materia,
        grupo: req.body.grupo,
        profesor: req.body.profesor,
        periodo: req.body.periodo,
    }
    conexion.query(sql, [data, id_curso],async (error, results) => {
        if (error) { 
            console.log(error);
            res.render('_shared/ventana', {
                alert: true,
                alertTitle: "Algo salió mal 😔",
                alertMessage: "El curso no pudo ser actualizado, inténtelo nuevamente.",
                alertIcon: "danger",
                showConfirmButton: false,
                timer: 1200,
                ruta: `actualizar_curso?id=${id_curso}&c=${req.body.carrera}`
            });
        }
        res.render('_shared/ventana', {
            alert: true,
            alertTitle: "¡Éxito! 💯",
            alertMessage: "El curso se actualizó correctamente.",
            alertIcon: "success",
            showConfirmButton: false,
            timer: 1000,
            ruta: `actualizar_curso?id=${id_curso}&c=${req.body.carrera}`
        });
    })
}
exports.eliminar_curso = async (req, res) => {
    let sql = `DELETE FROM cursos WHERE id_curso = ?`
    let id_curso = req.body.id_curso
    conexion.query(sql, [id_curso],async (error, results) => {
        if (error) { 
            console.log(error)
            res.json({ result: false })
        }
        res.json({ result: true })
    })
}

exports.registrarAlumnoCurso = async (req, res) => {
    let sql = `SELECT * FROM cursos_detalle WHERE curso = ? and alumno = ?`;
    let curso = parseInt(req.body.c);
    let alumno = req.body.a;
    let response = {
        success: false,
        msg: '',
        data: null
    }
    conexion.query(sql, [curso, alumno], async (error, results) => {
        if(results.length == 0){ //Si no está registrado en el curso
            //Lo registramos
            sql = `INSERT INTO cursos_detalle(curso,alumno,calificacion) VALUES(?,?,0)`
            conexion.query(sql, [curso, alumno], async (error, results) => {
                if(error != null){ //Si se presenta un error lo indicamos...
                    response.msg = "No se pudo completar esta operación: "+error.message                   
                }
                response.success = true;
                res.json(response);
            })
        }else{ //Si ya está registrado le indicamos...
            response.msg = "Ya te encuentras registrado en este curso, revisa en tu sección <Mis cursos>";
            res.json(response);
        }
    })
}

exports.obtenerCursosAlumno = async (req, res, next) => {
    let periodo = parseInt(req.body.p);
    let usuario = req.body.u;
    let sql = `SELECT m.nombre_materia as materia, g.nombre as grupo, cd.calificacion FROM cursos as c
                INNER JOIN materias as m ON c.materia = m.id_materia
                INNER JOIN grupos as g ON c.grupo = g.id_grupo
                INNER JOIN cursos_detalle as cd ON c.id_curso = cd.curso
                WHERE c.periodo = ? AND cd.alumno = ?`
    let response = {
        success: false,
        msg: '',
        data: null
    }
    conexion.query(sql, [periodo, usuario], async (error, results) => {
        if(error != null){
            response.msg = error.message;
        }else{
            response.success = true;
            response.data = results;
        }
        res.json(response);
    })
}

exports.obtenerCursosProfesor = async (req, res, next) =>
{
    let periodo = parseInt(req.body.p);
    let profesor = req.body.u;
    let sql = `SELECT c.id_curso, m.nombre_materia as materia, g.nombre as grupo FROM cursos as c
                INNER JOIN materias as m ON c.materia = m.id_materia
                INNER JOIN grupos as g ON c.grupo = g.id_grupo
                INNER JOIN profesores as p ON c.profesor = p.num_profesor
                WHERE c.periodo = ? AND p.id_usuario = ?`
    let response = {
        success: false,
        msg: '',
        data: null
    }
    conexion.query(sql, [periodo, profesor], async (error, results) =>
    {
        if (error != null)
        {
            response.msg = error.message;
        } else
        {
            response.success = true;
            response.data = results;
        }
        res.json(response);
    })
}

exports.obtenerAlumnosCurso = async (req, res, next) =>
{
    let id_curso = parseInt(req.body.cid);
    let sql = ` SELECT 
                    a.id_usuario as matricula, 
                    CONCAT(a.ap_paterno, ' ', a.ap_materno, ' ', a.nombre) as nombre,
                    cd.calificacion
                FROM cursos_detalle as cd
                INNER JOIN alumnos as a ON a.id_usuario = cd.alumno
                WHERE cd.curso = ?`
    let response = {
        success: false,
        msg: '',
        data: null
    }
    conexion.query(sql, [id_curso], async (error, results) =>
    {
        if (error != null)
        {
            response.msg = error.message;
        } else
        {
            response.success = true;
            response.data = results;
        }
        res.json(response);
    })
}
exports.obtenerAlumnosCursoEmpresa = async (req, res, next) =>
{
    let id_carrera = parseInt(req.body.c);
    let materia = req.body.m;
    let sql = ` SELECT 
                    a.id_usuario as matricula,
                    a.nombre,
                    a.ap_paterno,
                    a.ap_materno, 
                    ca.nombre as carrera,
                    m.nombre_materia as materia,
                    cd.calificacion
                FROM cursos_detalle as cd
                INNER JOIN cursos as c ON c.id_curso = cd.curso
                INNER JOIN alumnos as a ON a.id_usuario = cd.alumno
                INNER JOIN materias as m ON m.id_materia = c.materia
                INNER JOIN carreras as ca ON m.id_carrera = ca.id_carrera
                WHERE m.id_materia = ? AND ca.id_carrera = ?`
    let response = {
        success: false,
        msg: '',
        data: null
    }
    conexion.query(sql, [materia, id_carrera], async (error, results) =>
    {
        if (error != null)
        {
            response.msg = error.message;
        } else
        {
            response.success = true;
            response.data = results;
        }
        res.json(response);
    })
}

exports.actualizarCalificacionAlumno = async (req,res,next) => {
    let cid = parseInt(req.body.cid);
    let m = req.body.m;
    let c = parseInt(req.body.c);
    let sql = ` UPDATE
                    cursos_detalle AS cd
                SET
                    calificacion = ?
                WHERE
                    cd.curso = ? AND cd.alumno = ?`
    let response = {
        success: false,
        msg: '',
        data: null
    }
    conexion.query(sql, [c,cid,m], async (error, results) =>
    {
        if (error != null)
        {
            response.msg = error.message;
        } else
        {
            response.success = true;
            response.data = results;
        }
        res.json(response);
    })
}