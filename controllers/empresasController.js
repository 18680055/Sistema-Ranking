const conexion = require('../db/db');
const crypto = require('crypto-js');

function obtenerContrasena(contra){
    const desencriptar = crypto.AES.decrypt(contra, process.env.FIRMA);
    return desencriptar.toString(crypto.enc.Utf8);
}

exports.registrar_empresa = async (req, res, next) => {
    let bodyUsuarios = {
        id_usuario: req.body.id_usuario,
        contra: crypto.AES.encrypt(req.body.contra, "firma"),
        rol: req.body.rol,
        estatus: req.body.estatus
    }

    let bodyEmpresas = {
        id_usuario: req.body.id_usuario,
        nombre: req.body.nombre,
        email: req.body.email,
        telefono: req.body.telefono,
        direccion: req.body.direccion,
        tipo_sector: req.body.tipo_sector,
    }

    let sql = 'SELECT * FROM usuarios WHERE id_usuario = ?'
    conexion.query(sql, [bodyUsuarios.id_usuario], async (error, results) => {
        if (results.length > 0) {
            res.render('_shared/ventana', {
                alert: true,
                alertTitle: "Algo salió mal 😔",
                alertMessage: "La empresa no pudo ser registrada porque ya existe, inténtelo nuevamente.",
                alertIcon: "danger",
                showConfirmButton: false,
                timer: 1200,
                ruta: 'registrar_empresa'
            });
        }
        sql = `SELECT * FROM empresas WHERE id_usuario = ?`
        conexion.query(sql, [bodyEmpresas.id_usuario], async (error, results) => {
            if (results.length > 0) {
                res.render('_shared/ventana', {
                    alert: true,
                    alertTitle: "Algo salió mal 😔",
                    alertMessage: "La empresa no pudo ser registrada porque ya existe, inténtelo nuevamente.",
                    alertIcon: "danger",
                    showConfirmButton: false,
                    timer: 1200,
                    ruta: 'registrar_empresa'
                }); 
            }
            sql = `INSERT INTO usuarios SET ?`
            conexion.query(sql, bodyUsuarios, async (error, results) => {
                if (error != null) {
                    res.render('_shared/ventana', {
                        alert: true,
                        alertTitle: "Algo salió mal 😔",
                        alertMessage: "La empresa no pudo ser registrada "+error.message,
                        alertIcon: "danger",
                        showConfirmButton: false,
                        timer: 1200,
                        ruta: 'registrar_empresa'
                    });   
                }
                sql = `INSERT INTO empresas SET ?`
                conexion.query(sql, bodyEmpresas, async (error, results) => {
                    if (error != null) {
                        res.render('_shared/ventana', {
                            alert: true,
                            alertTitle: "Algo salió mal 😔",
                            alertMessage: "La empresa no pudo ser registrada " + error.message,
                            alertIcon: "danger",
                            showConfirmButton: false,
                            timer: 1200,
                            ruta: 'registrar_empresa'
                        });
                    }
                    res.render('_shared/ventana', {
                        alert: true,
                        alertTitle: "¡Éxito! 💯",
                        alertMessage: "La empresa se registró correctamente.",
                        alertIcon: "success",
                        showConfirmButton: false,
                        timer: 1000,
                        ruta: 'empresas'
                    });
                })
            })
        })
    })
}

exports.obtenerEmpresas = async (req, res, next) => {
    conexion.query('SELECT * FROM empresas', async (error, results) =>
    {
        if (results.length == 0)
        {
            req.empresas = new Array()
            return next()
        }
        req.empresas = results
        return next()
    })
}

exports.obtenerEmpresa = async (req, res, next) => {
    let id_usuario = req.query.id;
    let sql = `SELECT e.*, u.contra, u.rol, u.estatus FROM empresas as e INNER JOIN usuarios as u ON e.id_usuario = u.id_usuario WHERE e.id_usuario = ?`
    conexion.query(sql, [id_usuario], async (error, results) =>
    {
        if (results.length == 0)
        {
            req.empresa = undefined
            return next()
        }
        req.empresa = results[0]
        req.empresa.contra = obtenerContrasena(req.empresa.contra)
        return next()
    })
}

exports.actualizar_empresa = async (req, res) => {
    let sql = `UPDATE usuarios SET ? WHERE id_usuario = ?`
    let anterior_id_usuario = req.body.anterior_id_usuario

    let ContraHash = crypto.AES.encrypt(req.body.contra, "firma",);

    let bodyUsuarios = {
        id_usuario: req.body.id_usuario,
        contra: ContraHash,
        rol: req.body.rol,
        estatus: req.body.estatus
    }

    let bodyEmpresas = {
        id_usuario: req.body.id_usuario,
        nombre: req.body.nombre,
        email: req.body.email,
        telefono: req.body.telefono,
        direccion: req.body.direccion,
        tipo_sector: req.body.tipo_sector,
    }
    conexion.query(sql, [bodyUsuarios, anterior_id_usuario], async (error, results) =>
    {
        if (error != null)
        {
            console.log(error);
            res.render('_shared/ventana', {
                alert: true,
                alertTitle: "Algo salió mal 😔",
                alertMessage: "La empresa no pudo ser actualizada, inténtelo nuevamente.",
                alertIcon: "danger",
                showConfirmButton: false,
                timer: 1200,
                ruta: 'actualizar_empresa?id=' + anterior_id_usuario
            });
        }
        sql = `UPDATE empresas SET ? WHERE id_usuario = ?`
        conexion.query(sql, [bodyEmpresas, bodyEmpresas.id_usuario], async (error, results) =>{
            if (error != null)
            {
                console.log(error);
                res.render('_shared/ventana', {
                    alert: true,
                    alertTitle: "Algo salió mal 😔",
                    alertMessage: "La empresa no pudo ser actualizada, inténtelo nuevamente.",
                    alertIcon: "danger",
                    showConfirmButton: false,
                    timer: 1200,
                    ruta: 'actualizar_empresa?id=' + bodyEmpresas.id_usuario
                });
            }
            res.render('_shared/ventana', {
                alert: true,
                alertTitle: "¡Éxito! 💯",
                alertMessage: "La empresa se actualizó correctamente.",
                alertIcon: "success",
                showConfirmButton: false,
                timer: 1000,
                ruta: 'actualizar_empresa?id=' + bodyEmpresas.id_usuario
            });
        })
    })
}
exports.eliminar_empresa = async (req, res) =>
{
    let sql = `DELETE FROM empresas WHERE id_usuario = ?`
    let id_usuario = req.body.id_usuario
    conexion.query(sql, [id_usuario], async (error, results) =>
    {
        if (error != null)
        {
            console.log(error)
            res.json({ result: false })
        }
        let sql = `DELETE FROM usuarios WHERE id_usuario = ?`
        conexion.query(sql, [id_usuario], async (error, results) =>
        {
            if (error != null)
            {
                console.log(error)
                res.json({ result: false })
            }

            res.json({ result: true })
        })
    })
}