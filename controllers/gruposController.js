const conexion = require('../db/db')
const crypto = require('crypto-js')

exports.listarGrupos = async (req, res, next) => {
    let sql = `SELECT g.id_grupo,g.nombre,c.nombre as carrera,g.semestre FROM grupos as g INNER JOIN carreras as c ON g.carrera = c.id_carrera`
    conexion.query(sql, async (error, results) => {
        req.grupos = results
        if(!results){
            req.grupos = new Array()
        }
        return next()
    })
};

exports.listarGruposPorCarrera = async (req, res, next) => {
    let c = req.query.c;
    let sql = `SELECT g.id_grupo,g.nombre,c.nombre as carrera,g.semestre FROM grupos as g INNER JOIN carreras as c ON g.carrera = c.id_carrera WHERE g.carrera = ?`
    conexion.query(sql, [c], async (error, results) => {
        req.grupos = results
        if(!results){
            req.grupos = new Array()
        }
        return next()
    })
};

exports.registrar_grupo = async (req, res, next) => {
    let sql = 'INSERT INTO grupos SET ?'
    let data = req.body
    conexion.query(sql, data, async (error, results) => {
        if (error) {
            console.log(error);
            res.render('_shared/ventana', {
                alert: true,
                alertTitle: "Algo salió mal 😔",
                alertMessage: "El grupo no pudo ser registrado, inténtelo nuevamente.",
                alertIcon: "danger",
                showConfirmButton: false,
                timer: 1200,
                ruta: 'registrar_grupo'
            });
        }
        res.render('_shared/ventana', {
            alert: true,
            alertTitle: "¡Éxito! 💯",
            alertMessage: "El grupo se registró correctamente.",
            alertIcon: "success",
            showConfirmButton: false,
            timer: 1000,
            ruta: 'grupos'
        });
    })
};

exports.eliminar_grupo = async (req, res) => {
    let sql = `DELETE FROM grupos WHERE id_grupo = ?`
    let id_grupo = req.body.id_grupo
    conexion.query(sql, [id_grupo],async (error, results) => {
        if (error) { 
            console.log(error)
            res.json({ result: false })
        }
        res.json({ result: true })
    })
};

exports.obtenerGrupo = async (req, res, next) => {
    let id_grupo = req.query.id;
    conexion.query('SELECT * FROM grupos WHERE id_grupo = ?', [id_grupo],async (error, results) => {
        if (!results) { 
            req.grupo = undefined
            return next() 
        }
        req.grupo = results[0]
        return next()
    })
};

exports.actualizar_grupo = async (req, res) => {
    let sql = `UPDATE grupos SET ? WHERE id_grupo = ?`
    let id_grupo = req.body.id_grupo
    let data = {
        nombre: req.body.nombre,
        carrera: req.body.carrera,
        semestre: req.body.semestre
    }
    conexion.query(sql, [data, id_grupo],async (error, results) => {
        if (error) { 
            console.log(error);
            res.render('_shared/ventana', {
                alert: true,
                alertTitle: "Algo salió mal 😔",
                alertMessage: "El grupo no pudo ser actualizado, inténtelo nuevamente.",
                alertIcon: "danger",
                showConfirmButton: false,
                timer: 1200,
                ruta: 'actualizar_grupo?id='+id_grupo
            });
        }
        res.render('_shared/ventana', {
            alert: true,
            alertTitle: "¡Éxito! 💯",
            alertMessage: "El grupo se actualizó correctamente.",
            alertIcon: "success",
            showConfirmButton: false,
            timer: 1000,
            ruta: 'actualizar_grupo?id='+id_grupo
        });
    })
};