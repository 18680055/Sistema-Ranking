const conexion = require('../db/db');
const crypto = require('crypto-js');

//metodo listar conteo de cuentas 
exports.conteoCuentas = async (req, res, next) => {

    conexion.query('SELECT COUNT(rol) AS Administradores FROM usuarios WHERE rol="Administrador";', async (error, results) => {
        if (!results) { return next() }
        req.admin = results[0]
        conexion.query('SELECT COUNT(rol) AS Alumnos FROM usuarios WHERE rol="Alumno";', async (error, results) => {
            if (!results) { return next() }
            req.alumno = results[0]
            conexion.query('SELECT COUNT(rol) AS Profesores FROM usuarios WHERE rol="Profesor";', async (error, results) => {
                if (!results) { return next() }
                req.profesor = results[0]
                conexion.query('SELECT COUNT(rol) AS Empresas FROM usuarios WHERE rol="Empresa";', async (error, results) => {
                    if (!results) { return next() }
                    req.empresa = results[0]

                    //console.log(req.admin," ",req.alumno," ",req.profesor," ",req.empresa)
                    return next();
                })
            })
        })
    })
}
//conteo alumnos de cada carrera
exports.conteoAlumnosCarrera = async (req, res, next) => {
    let sql = 'select count(id_usuario) as conteo, c.nombre as carrera from alumnos as a join carreras as c on a.id_carrera = c.id_carrera group by carrera'
    conexion.query(sql, async (error,results) => {
        if(error != null){
            console.log(error.message)
            return next()
        }
        req.conteoAlumnosCarrera = results
        // req.conteoAlumnosCarrera = Object.assign({}, ...results.map((x) => ({ [`${x.nombre}`]: 0 })))
        // let carreras = results
        // for (let carrera of carreras) {
        //     sql = 'SELECT COUNT(id_usuario) as conteo FROM alumnos WHERE id_carrera = ?'
        //     req.conteoAlumnosCarrera[carrera.nombre] = conexion.query(sql, carrera.id_carrera, async (error, results) => results[0].conteo)
        // }
        return next()
    })
    // conexion.query('SELECT COUNT(carreras.id_carrera) AS carrera FROM alumnos INNER JOIN carreras ON alumnos.id_carrera = carreras.id_carrera WHERE carreras.nombre="Sistemas";',
    //     async (error, results) => {
    //         if (!results) { return next() }
    //         req.sistemas = results[0]
    //         conexion.query('SELECT COUNT(carreras.id_carrera) AS carrera FROM alumnos INNER JOIN carreras ON alumnos.id_carrera = carreras.id_carrera WHERE carreras.nombre="Industrial";',
    //             async (error, results) => {
    //                 if (!results) { return next() }
    //                 req.industrial = results[0]
    //                 conexion.query('SELECT COUNT(carreras.id_carrera) AS carrera FROM alumnos INNER JOIN carreras ON alumnos.id_carrera = carreras.id_carrera WHERE carreras.nombre="Mecatronica";',
    //                     async (error, results) => {
    //                         if (!results) { return next() }
    //                         req.mecatronica = results[0]
    //                         conexion.query('SELECT COUNT(carreras.id_carrera) AS carrera FROM alumnos INNER JOIN carreras ON alumnos.id_carrera = carreras.id_carrera WHERE carreras.nombre="Electronica";',
    //                             async (error, results) => {
    //                                 if (!results) { return next() }
    //                                 req.electronica = results[0]
    //                                 conexion.query('SELECT COUNT(carreras.id_carrera) AS carrera FROM alumnos INNER JOIN carreras ON alumnos.id_carrera = carreras.id_carrera WHERE carreras.nombre="Empresarial";',
    //                                     async (error, results) => {
    //                                         if (!results) { return next() }
    //                                         req.empresarial = results[0]
    //                                         conexion.query('SELECT COUNT(carreras.id_carrera) AS carrera FROM alumnos INNER JOIN carreras ON alumnos.id_carrera = carreras.id_carrera WHERE carreras.nombre="Contador Publico";',
    //                                             async (error, results) => {
    //                                                 if (!results) { return next() }
    //                                                 req.contador = results[0]
    //                                                 return next();
    //                                             })
    //                                     })
    //                             })
    //                     })
    //             })
    //         //console.log(req.sistemas)

    //     })
}