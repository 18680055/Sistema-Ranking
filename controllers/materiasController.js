const conexion = require('../db/db');
const crypto = require('crypto-js');

exports.listaMaterias = async (req, res, next) => {
    conexion.query('SELECT m.id_materia as id, m.nombre_materia as nombre, c.nombre as carrera FROM materias as m INNER JOIN carreras as c ON m.id_carrera = c.id_carrera',async (error, results) => {
        if (!results) { return next() }
        req.materias = results
        return next();
    })
}

exports.listaMateriasPorCarrera = async (req, res, next) => {
    let c = req.query.c;
    conexion.query('SELECT m.id_materia as id, m.nombre_materia as nombre, c.nombre as carrera FROM materias as m INNER JOIN carreras as c ON m.id_carrera = c.id_carrera WHERE m.id_carrera = ?',[c],async (error, results) => {
        if (!results) { return next() }
        req.materias = results
        return next();
    })
}

exports.obtenerMateria = async (req, res, next) => {
    let id = req.query.id
    conexion.query('SELECT m.id_materia, m.nombre_materia, c.id_carrera FROM materias as m INNER JOIN carreras as c ON m.id_carrera = c.id_carrera WHERE m.id_materia = ?', [id],async (error, results) => {
        if (!results || results.length == 0) { 
            res.render('_shared/ventana', {
                alert: true,
                alertTitle: "Algo salió mal 😔",
                alertMessage: "La materia no pudo ser encontrada, inténtelo nuevamente.",
                alertIcon: "warning",
                showConfirmButton: false,
                timer: 1200,
                ruta: 'materias'
            });
        }
        req.materia = results[0]
        return next()
    })
}

exports.registrar_materia = async (req, res, next) => {
    let sql = `INSERT INTO materias SET ?`
    let data = {
        id_materia: req.body.id_materia,
        nombre_materia: req.body.nombre_materia,
        id_carrera: parseInt(req.body.carrera)
    }
    conexion.query(sql, data,async (error, results) => {
        if (error) { 
            console.log(error);
            res.render('_shared/ventana', {
                alert: true,
                alertTitle: "Algo salió mal 😔",
                alertMessage: "La materia no pudo ser registrada, inténtelo nuevamente.",
                alertIcon: "danger",
                showConfirmButton: false,
                timer: 1200,
                ruta: 'registrar_materia'
            });
        }
        res.render('_shared/ventana', {
            alert: true,
            alertTitle: "¡Éxito! 💯",
            alertMessage: "La materia se registró correctamente.",
            alertIcon: "success",
            showConfirmButton: false,
            timer: 1000,
            ruta: 'materias'
        });
    })
}

exports.actualizar_materia = async (req, res, next) => {
    let sql = `UPDATE materias SET ? WHERE id_materia = ?`
    let id_materia = req.body.id_materia
    let data = {
        nombre_materia: req.body.nombre_materia,
        id_carrera: parseInt(req.body.carrera)
    }
    conexion.query(sql, [data, id_materia],async (error, results) => {
        if (error) { 
            console.log(error);
            res.render('_shared/ventana', {
                alert: true,
                alertTitle: "Algo salió mal 😔",
                alertMessage: "La materia no pudo ser actualizada, inténtelo nuevamente.",
                alertIcon: "danger",
                showConfirmButton: false,
                timer: 1200,
                ruta: 'actualizar_materia?id='+id_materia
            });
        }
        res.render('_shared/ventana', {
            alert: true,
            alertTitle: "¡Éxito! 💯",
            alertMessage: "La materia se actualizó correctamente.",
            alertIcon: "success",
            showConfirmButton: false,
            timer: 1000,
            ruta: 'actualizar_materia?id='+id_materia
        });
    })
}

exports.eliminar_materia = async (req, res) => {
    let sql = `DELETE FROM materias WHERE id_materia = ?`
    let id_materia = req.body.id_materia
    console.log(id_materia)
    conexion.query(sql, [id_materia],async (error, results) => {
        if (error) { 
            console.log(error)
            res.json({ result: false })
        }
        res.json({ result: true })
    })
}
