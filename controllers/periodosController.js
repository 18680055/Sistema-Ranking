const conexion = require('../db/db');
const crypto = require('crypto-js');

exports.registrar_periodo = async (req, res, next) => {
    let sql = 'INSERT INTO periodos SET ?'
    let data = req.body
    conexion.query(sql, data, async (error, results) => {
        if (error) {
            console.log(error);
            res.render('_shared/ventana', {
                alert: true,
                alertTitle: "Algo salió mal 😔",
                alertMessage: "El periodo no pudo ser registrado, inténtelo nuevamente.",
                alertIcon: "danger",
                showConfirmButton: false,
                timer: 1200,
                ruta: 'registrar_periodo'
            });
        }
        res.render('_shared/ventana', {
            alert: true,
            alertTitle: "¡Éxito! 💯",
            alertMessage: "El periodo se registró correctamente.",
            alertIcon: "success",
            showConfirmButton: false,
            timer: 1000,
            ruta: 'periodos'
        });
    })
}

exports.obtenerPeriodos = async (req, res, next) => {
    conexion.query('SELECT id_periodo, descripcion, desde, hasta FROM periodos WHERE estatus = 1 and eliminado = 0',async (error, results) => {
        if (!results) { 
            req.periodos = new Array() 
            return next() 
        }
        req.periodos = results
        return next()
    })
}

exports.obtenerPeriodo = async (req, res, next) => {
    let id_periodo = req.query.id;
    conexion.query('SELECT * FROM periodos WHERE id_periodo = ?', [id_periodo],async (error, results) => {
        if (!results) { 
            req.periodo = undefined
            return next() 
        }
        req.periodo = results[0]
        return next()
    })
}
exports.actualizar_periodo = async (req, res) => {
    let sql = `UPDATE periodos SET ? WHERE id_periodo = ?`
    let id_periodo = req.body.id_periodo
    let data = {
        nombre: req.body.nombre,
        coordinador: req.body.coordinador,
        correo: req.body.correo
    }
    conexion.query(sql, [data, id_periodo],async (error, results) => {
        if (error) { 
            console.log(error);
            res.render('_shared/ventana', {
                alert: true,
                alertTitle: "Algo salió mal 😔",
                alertMessage: "El periodo no pudo ser actualizado, inténtelo nuevamente.",
                alertIcon: "danger",
                showConfirmButton: false,
                timer: 1200,
                ruta: 'actualizar_periodo?id='+id_periodo
            });
        }
        res.render('_shared/ventana', {
            alert: true,
            alertTitle: "¡Éxito! 💯",
            alertMessage: "El periodo se actualizó correctamente.",
            alertIcon: "success",
            showConfirmButton: false,
            timer: 1000,
            ruta: 'actualizar_periodo?id='+id_periodo
        });
    })
}
exports.eliminar_periodo = async (req, res) => {
    let sql = `UPDATE periodos SET estatus = 0, eliminado = 1 WHERE id_periodo = ?`
    let id_periodo = req.body.id_periodo
    conexion.query(sql, [id_periodo],async (error, results) => {
        if (error) { 
            console.log(error)
            res.json({ result: false })
        }
        res.json({ result: true })
    })
}