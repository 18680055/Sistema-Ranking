const conexion = require('../db/db');
const crypto = require('crypto-js');

//metodo Registrar Profesores
exports.registrar_profesores = async (req, res) =>
{
    try
    {
        const id_usuario = req.body.id_usuario
        const rol = req.body.rol
        const contra = req.body.contra
        const ContraHash = crypto.AES.encrypt(contra, "firma",);
        // let ContraHash=await bycryptjs.hash(contra, 8);
        const estatus = req.body.estatus
        const nombre = req.body.nombre
        const ap_paterno = req.body.ap_paterno
        const ap_materno = req.body.ap_materno
        const cedula = req.body.cedula
        const correo = req.body.correo
        const grado_academico = req.body.grado_academico
        const tipo_contrato = req.body.tipo_contrato
        const tipo_academia = req.body.tipo_academia


        conexion.query('SELECT*FROM usuarios WHERE id_usuario= ?', [id_usuario], async (error, results) =>
        {
            if (error)
            {
                console.log(error)
            }
            if (results.length == 1)
            {
                res.render('_shared/ventana', {
                    alert: true,
                    alertTitle: "Advertencia",
                    alertMessage: "USUARIO YA EXISTENTE!!!",
                    alertIcon: "error",
                    showConfirmButton: false,
                    timer: 1000,
                    ruta: 'registrar_profesor'
                });

            } else
            {
                conexion.query('INSERT INTO usuarios SET ?', {
                    id_usuario: id_usuario,
                    contra: ContraHash,
                    rol: rol,
                    estatus: estatus
                },
                    async (error, result) =>
                    {
                        if (error)
                        {
                            console.log(error)
                        }

                        conexion.query('INSERT INTO profesores SET ?', {
                            id_usuario: id_usuario,
                            nombre: nombre,
                            ap_paterno: ap_paterno,
                            ap_materno: ap_materno,
                            cedula: cedula,
                            correo: correo,
                            tipo_contrato: tipo_contrato,
                            grado_academico: grado_academico,
                            tipo_academia: tipo_academia
                        }, async (error, result) =>
                        {
                            if (error)
                            {
                                console.log(error)
                            }
                        });
                        if (rol == "Administrador")
                        {
                            res.render('_shared/ventana', {
                                alert: true,
                                alertTitle: "REGISTRO EXITOSO!!!",
                                alertMessage: "USUARIO REGISTRADO",
                                alertIcon: "success",
                                showConfirmButton: false,
                                timer: 1000,
                                ruta: 'listar_administradores'
                            });
                        } else
                        {
                            res.render('_shared/ventana', {
                                alert: true,
                                alertTitle: "REGISTRO EXITOSO!!!",
                                alertMessage: "USUARIO REGISTRADO",
                                alertIcon: "success",
                                showConfirmButton: false,
                                timer: 1000,
                                ruta: 'listar_profesores'
                            });
                        }
                        //res.redirect('/listar_alumnos');

                    }
                );
            }
        })
    } catch (error)
    {
        console.log(error)
    }
}
//metodo listar profesores
exports.listaProfesores = async (req, res, next) =>
{
    conexion.query('SELECT usuarios.id_usuario, profesores.num_profesor, profesores.nombre, profesores.ap_paterno, profesores.ap_materno, profesores.cedula, profesores.correo,profesores.tipo_academia FROM profesores INNER JOIN usuarios ON usuarios.id_usuario=profesores.id_usuario WHERE usuarios.rol="Profesor"; ', async (error, results) =>
    {
        if (!results)
        {
            return next()
        }
        req.profesores = results
        //console.log(req.profesores)
        return next();
    })
}
//metodo visualizar informacion de un profesor
exports.infoProfesor = async (req, res, next) =>
{
    try
    {
        const id_profesor = req.query.id
        conexion.query('SELECT p.id_usuario, p.nombre, p.ap_paterno, p.ap_materno, p.cedula, p.correo, p.tipo_contrato, p.grado_academico, p.tipo_academia FROM profesores as p WHERE p.id_usuario=?;', [id_profesor], async (error, results) =>
        {
            if (!results)
            {
                return next()
            }
            req.profesor = results[0]
            conexion.query('SELECT contra,estatus FROM usuarios WHERE id_usuario = ?', [id_profesor], async (error, results) =>
            {
                req.profesor.contra_encrypt = results[0].contra;
                let contra_decrypt = crypto.AES.decrypt(results[0].contra, "firma");
                req.profesor.contra = contra_decrypt.toString(crypto.enc.Utf8);
                req.profesor.estatus = results[0].estatus;
                return next();
            });
        });
    } catch (error)
    {
        console.log(error)
    }
}
exports.actualizar_profesor = async (req, res, next) =>
{
    try
    {
        let sql = '';
        let data = [];
        const id_usuario = req.body.id_usuario;
        const rol = req.body.rol;
        const contra = req.body.contra;
        const estatus = req.body.estatus;
        const nombre = req.body.nombre;
        const ap_paterno = req.body.ap_paterno;
        const ap_materno = req.body.ap_materno;
        const cedula = req.body.cedula;
        const correo = req.body.correo;
        const grado_academico = req.body.grado_academico;
        const tipo_contrato = req.body.tipo_contrato;
        const tipo_academia = req.body.tipo_academia;
        const ContraHash = crypto.AES.encrypt(contra, process.env.FIRMA)

        sql = `UPDATE usuarios SET contra = ?, rol = ?, estatus = ? WHERE id_usuario = ?`;
        data = [ContraHash.toString(), rol, estatus, id_usuario];
        conexion.query(sql, data, async (error, results) =>
        {
            if (error)
            {
                res.render('_shared/ventana', {
                    alert: true,
                    alertTitle: "ALGO SALIO MAL!!!",
                    alertMessage: error.message,
                    alertIcon: "error",
                    showConfirmButton: false,
                    timer: 1000,
                    ruta: 'listar_profesores'
                });
            }
            sql = `UPDATE profesores SET 
                        nombre = ?, 
                        ap_paterno = ?, 
                        ap_materno = ?, 
                        cedula = ?, 
                        grado_academico = ?,
                        correo = ?,
                        tipo_contrato = ?,
                        tipo_academia = ?
                    WHERE id_usuario = ?`;
            data = [nombre, ap_paterno, ap_materno, cedula, grado_academico, correo, tipo_contrato, tipo_academia, id_usuario];
            conexion.query(sql, data, async (error, results) =>
            {
                if (error)
                {
                    console.log(error)
                }
                res.render('_shared/ventana', {
                    alert: true,
                    alertTitle: "ACTUALIZACIÓN EXITOSA!!!",
                    alertMessage: "USUARIO ACTUALIZADO",
                    alertIcon: "success",
                    showConfirmButton: false,
                    timer: 1000,
                    ruta: 'listar_profesores'
                });
            });
        });

    } catch (error)
    {
        res.render('_shared/ventana', {
            alert: true,
            alertTitle: "ALGO SALIO MAL!!!",
            alertMessage: error.message,
            alertIcon: "error",
            showConfirmButton: false,
            timer: 1000,
            ruta: 'listar_profesores'
        });
    }
}

exports.eliminar_profesor = async (req,res) => {
    let id = req.query.id
    let sql = `DELETE FROM profesores WHERE id_usuario = ?`
    conexion.query(sql, [id], async (error, results) => {
        if (error) {
            res.render('_shared/ventana', {
                alert: true,
                alertTitle: "ALGO SALIO MAL!!!",
                alertMessage: error.message,
                alertIcon: "error",
                showConfirmButton: false,
                timer: 1000,
                ruta: 'listar_profesores'
            });
        }
        let sql = `DELETE FROM usuarios WHERE id_usuario = ?`
        conexion.query(sql, [id], async (error, results) => {
            if (error){
                res.render('_shared/ventana', {
                    alert: true,
                    alertTitle: "ALGO SALIO MAL!!!",
                    alertMessage: error.message,
                    alertIcon: "error",
                    showConfirmButton: false,
                    timer: 1000,
                    ruta: 'listar_profesores'
                });
            }
            res.render('_shared/ventana', {
                alert: true,
                alertTitle: "ELIMINACION EXITOSA!!!",
                alertMessage: "PROFESOR ELIMINADO",
                alertIcon: "success",
                showConfirmButton: false,
                timer: 1000,
                ruta: 'listar_profesores'
            });
        })
    })
}