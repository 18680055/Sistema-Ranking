const mysql=require('mysql');

const conexion= mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_DATABASE
});
conexion.connect( (error)=>{
    if(error){
        console.log('El Error de Conexion es: '+error)
        return
    }
    console.log('Conectado a la Base de Datos')
});
module.exports=conexion;