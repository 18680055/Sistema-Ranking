
/**
 * @author: obejashaundev
 * @description: Habilitar los tooltips de bootstrap en el sistema.
 */
var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'));
var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
  return new bootstrap.Tooltip(tooltipTriggerEl)
});
/*----------------------------------------------------------------- */
//Tabla listar alumnos
$(document).ready(function() {
 
  // Tabla Lista de Alumnos
  $('#listaAlumnos').DataTable( {
    responsive:"true",
   dom:"Bfrtilp'",
    buttons: [
      '<a class="btn btn-success" href="/registrar_alumnos"><i class="bi bi-plus"></i> Nuevo alumno</a>',
      {
        extend: "copy",
        text: '<i class="bi bi-clipboard"></i> Copiar',
        className: 'btn btn-warning btn-group ',
        copytitle:"inforamcion cpiada"
    },  
        {
        extend: 'pdfHtml5',
          text: '<i class="bi bi-filetype-pdf"></i> PDF',
        className: 'btn btn-danger btn-group nav-item',
        filename: 'Lista de Alumnos',
        title:'Cuentas de Alumnos',
        messageTop: function(){
          momentoActual= new Date();
          hora=momentoActual.getHours();
          minto=momentoActual.getMinutes();
          seundo=momentoActual.getSeconds();
          result = momentoActual.toLocaleString();
         
         return "Lista Actualizada: "+result;
        }
        }, 
    ],
    language: { "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json",
    "buttons": {
      "copyTitle": 'Informacion copiada',
      "copyKeys": 'Use your keyboard or menu to select the copy command',
      "copySuccess": {
          "_": '%d filas copiadas al portapapeles',
          "1": '1 fila copiada al portapapeles'
      }}
    }
  } );

  // Tabla Lista de Profesores
  $('#listaProfesores').DataTable( {
    responsive:"true",
   dom:"Bfrtilp'",
    buttons: [
      '<a class="btn btn-primary" href="/registrar_profesor"><i class="bi bi-person-add"></i> Nuevo Profesor</a>',
      {
        extend: "copy",
        text: '<i class="bi bi-clipboard"></i> Copiar',
        className: 'btn btn-warning',
        copytitle:"Información en el portapapeles",
        exportOptions: {
          modifier: {
              page: 'current'
          }
        }
    },  
        {
        extend: 'pdf',
        text: '<i class="bi bi-filetype-pdf"></i> Guardar en PDF',
        className: 'btn btn-danger',
        filename: 'Lista de Profesores',
        title:'Lista de Profesores',
        exportOptions:{
          modifier: {
              page: 'current'
          }
        },
        messageTop: function(){
          momentoActual= new Date();
          hora=momentoActual.getHours();
          minto=momentoActual.getMinutes();
          seundo=momentoActual.getSeconds();
          result = momentoActual.toLocaleString();
         
         return "Lista Actualizada: "+result;
        }
        }, 
    ],
    language: { "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json",
    "buttons": {
      "copyTitle": 'Informacion copiada',
      "copyKeys": 'Use your keyboard or menu to select the copy command',
      "copySuccess": {
          "_": '%d filas copiadas al portapapeles',
          "1": '1 fila copiada al portapapeles'
      }}
    }
  } );
  
  // Tabla Lista de Administradores
  $('#listaAdministradores').DataTable( {
    responsive:"true",
   dom:"Bfrtilp'",
    buttons: [
      '<a class="btn btn-success" href="/registrar_administradores"><i class="bi bi-plus"></i> Nuevo Administrador</a>',
      {
        extend: "copy",
        text: '<i class="bi bi-clipboard"></i> Copiar',
        className: 'btn btn-warning btn-group ',
        copytitle:"inforamcion cpiada"
    },  
        {
        extend: 'pdfHtml5',
        text: '<i class="bi bi-filetype-pdf"></i> PDF',
        className: 'btn btn-danger btn-group nav-item',
        filename: 'Lista de Administradores',
        title:'Cuentas de Administradores',
        messageTop: function(){
          momentoActual= new Date();
          hora=momentoActual.getHours();
          minto=momentoActual.getMinutes();
          seundo=momentoActual.getSeconds();
          result = momentoActual.toLocaleString();
         
         return "Lista Actualizada: "+result;
        }
        }, 
    ],
    language: { "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json",
    "buttons": {
      "copyTitle": 'Informacion copiada',
      "copyKeys": 'Use your keyboard or menu to select the copy command',
      "copySuccess": {
          "_": '%d filas copiadas al portapapeles',
          "1": '1 fila copiada al portapapeles'
      }}
    }
  } );

  $('#listaMaterias').DataTable({
    responsive:"true",
    dom:"Bfrtilp'",
    buttons: [
        '<a class="btn btn-primary" href="/registrar_materia"><i class="bi bi-journal-plus"></i> Nueva Materia</a>',
        {
          extend: "copy",
          text: '<i class="bi bi-clipboard"></i> Copiar',
          className: 'btn btn-warning',
          copytitle:"Información en el portapapeles",
          exportOptions: {
              modifier: {
                  page: 'current'
              }
          }
        },  
        {
        extend: 'pdf',
        text: '<i class="bi bi-filetype-pdf"></i> Guardar en PDF',
        className: 'btn btn-danger',
        filename: 'Lista de Materias',
        title:'Lista de Materias',
        exportOptions:{
          modifier: {
              page: 'current'
          }
        },
        messageTop: function(){
          momentoActual= new Date();
          hora=momentoActual.getHours();
          minto=momentoActual.getMinutes();
          seundo=momentoActual.getSeconds();
          result = momentoActual.toLocaleString();
         
         return "Lista Actualizada: "+result;
        }
        }, 
    ],
    language: { "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json",
    "buttons": {
      "copyTitle": 'Información copiada',
      "copyKeys": 'Use your keyboard or menu to select the copy command',
      "copySuccess": {
          "_": '%d filas copiadas al portapapeles',
          "1": '1 fila copiada al portapapeles'
      }}
    }
  });

  $('#listaCarreras').DataTable({
    responsive:"true",
    dom:"Bfrtilp'",
    buttons: [
        '<a class="btn btn-primary" href="/registrar_carrera"><i class="bi bi-plus-circle"></i> Nueva Carrera</a>',
        {
          extend: "copy",
          text: '<i class="bi bi-clipboard"></i> Copiar',
          className: 'btn btn-warning',
          copytitle:"Información en el portapapeles",
          exportOptions: {
              modifier: {
                  page: 'current'
              }
          }
        },  
        {
        extend: 'pdf',
        text: '<i class="bi bi-filetype-pdf"></i> Guardar en PDF',
        className: 'btn btn-danger',
        filename: 'Lista de Carreras',
        title:'Lista de Carreras',
        exportOptions:{
          modifier: {
              page: 'current'
          }
        },
        messageTop: function(){
          momentoActual= new Date();
          hora=momentoActual.getHours();
          minto=momentoActual.getMinutes();
          seundo=momentoActual.getSeconds();
          result = momentoActual.toLocaleString();
         
         return "Lista Actualizada: "+result;
        }
        }, 
    ],
    language: { "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json",
    "buttons": {
      "copyTitle": 'Información copiada',
      "copyKeys": 'Use your keyboard or menu to select the copy command',
      "copySuccess": {
          "_": '%d filas copiadas al portapapeles',
          "1": '1 fila copiada al portapapeles'
      }}
    }
  });

  $('#listaGrupos').DataTable({
    responsive:"true",
    dom:"Bfrtilp'",
    buttons: [
        '<a class="btn btn-primary" href="/registrar_grupo"><i class="bi bi-folder-plus"></i> Nuevo grupo</a>',
        {
          extend: "copy",
          text: '<i class="bi bi-clipboard"></i> Copiar',
          className: 'btn btn-warning',
          copytitle:"Información en el portapapeles",
          exportOptions: {
              modifier: {
                  page: 'current'
              }
          }
        },  
        {
        extend: 'pdf',
        text: '<i class="bi bi-filetype-pdf"></i> Guardar en PDF',
        className: 'btn btn-danger',
        filename: 'Lista de Grupos',
        title:'Lista de Grupos',
        exportOptions:{
          modifier: {
              page: 'current'
          }
        },
        messageTop: function(){
          momentoActual= new Date();
          hora=momentoActual.getHours();
          minto=momentoActual.getMinutes();
          seundo=momentoActual.getSeconds();
          result = momentoActual.toLocaleString();
         
         return "Lista Actualizada: "+result;
        }
        }, 
    ],
    language: { "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json",
    "buttons": {
      "copyTitle": 'Información copiada',
      "copyKeys": 'Use your keyboard or menu to select the copy command',
      "copySuccess": {
          "_": '%d filas copiadas al portapapeles',
          "1": '1 fila copiada al portapapeles'
      }}
    }
  });

  $('#listaPeriodos').DataTable({
    responsive:"true",
    dom:"Bfrtilp'",
    buttons: [
        '<a class="btn btn-primary" href="/registrar_periodo"><i class="bi bi-folder-plus"></i> Nuevo periodo</a>',
        {
          extend: "copy",
          text: '<i class="bi bi-clipboard"></i> Copiar',
          className: 'btn btn-warning',
          copytitle:"Información en el portapapeles",
          exportOptions: {
              modifier: {
                  page: 'current'
              }
          }
        },  
        {
        extend: 'pdf',
        text: '<i class="bi bi-filetype-pdf"></i> Guardar en PDF',
        className: 'btn btn-danger',
        filename: 'Lista de Periodos',
        title:'Lista de Periodos',
        exportOptions:{
          modifier: {
              page: 'current'
          }
        },
        messageTop: function(){
          momentoActual= new Date();
          hora=momentoActual.getHours();
          minto=momentoActual.getMinutes();
          seundo=momentoActual.getSeconds();
          result = momentoActual.toLocaleString();
         
         return "Lista Actualizada: "+result;
        }
        }, 
    ],
    language: { "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json",
    "buttons": {
      "copyTitle": 'Información copiada',
      "copyKeys": 'Use your keyboard or menu to select the copy command',
      "copySuccess": {
          "_": '%d filas copiadas al portapapeles',
          "1": '1 fila copiada al portapapeles'
      }}
    }
  });

  $('#listaCursos').DataTable({
    responsive:"true",
    dom:"Bfrtilp'",
    buttons: [
        '<button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#carreraModal"><i class="bi bi-plus"></i> Nuevo curso</button>',
        {
          extend: "copy",
          text: '<i class="bi bi-clipboard"></i> Copiar',
          className: 'btn btn-warning',
          copytitle:"Información en el portapapeles",
          exportOptions: {
              modifier: {
                  page: 'current'
              }
          }
        },  
        {
        extend: 'pdf',
        text: '<i class="bi bi-filetype-pdf"></i> Guardar en PDF',
        className: 'btn btn-danger',
        filename: 'Lista de Profesores',
        title:'Lista de Profesores',
        exportOptions:{
          modifier: {
              page: 'current'
          }
        },
        messageTop: function(){
          momentoActual= new Date();
          hora=momentoActual.getHours();
          minto=momentoActual.getMinutes();
          seundo=momentoActual.getSeconds();
          result = momentoActual.toLocaleString();
         
         return "Lista Actualizada: "+result;
        }
        }, 
    ],
    language: { "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json",
    "buttons": {
      "copyTitle": 'Información copiada',
      "copyKeys": 'Use your keyboard or menu to select the copy command',
      "copySuccess": {
          "_": '%d filas copiadas al portapapeles',
          "1": '1 fila copiada al portapapeles'
      }}
    }
  });

$('#listaEmpresas').DataTable({
  responsive: "true",
  dom: "Bfrtilp'",
  buttons: [
    '<a class="btn btn-primary" href="/registrar_empresa"><i class="bi bi-plus"></i> Nueva empresa</a>',
    {
      extend: "copy",
      text: '<i class="bi bi-clipboard"></i> Copiar',
      className: 'btn btn-warning',
      copytitle: "Información en el portapapeles",
      exportOptions: {
        modifier: {
          page: 'current'
        }
      }
    },
    {
      extend: 'pdf',
      text: '<i class="bi bi-filetype-pdf"></i> Guardar en PDF',
      className: 'btn btn-danger',
      filename: 'Lista de Empresas',
      title: 'Lista de Empresas',
      exportOptions: {
        modifier: {
          page: 'current'
        }
      },
      messageTop: function()
      {
        momentoActual = new Date();
        hora = momentoActual.getHours();
        minto = momentoActual.getMinutes();
        seundo = momentoActual.getSeconds();
        result = momentoActual.toLocaleString();

        return "Lista Actualizada: " + result;
      }
    },
  ],
  language: {
    "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json",
    "buttons": {
      "copyTitle": 'Información copiada',
      "copyKeys": 'Use your keyboard or menu to select the copy command',
      "copySuccess": {
        "_": '%d filas copiadas al portapapeles',
        "1": '1 fila copiada al portapapeles'
      }
    }
  }
});

} );
//Funcion para mirar contraseña
function mirarContra() {
  var x = document.getElementById("contra");
  if (x.type == "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}

