const express = require('express');
const { body, validatorResult, Result } = require('express-validator');
const router = express.Router();
const accountController = require('../controllers/accountController');
const administradoresController = require('../controllers/administradoresController');
const alumnosController = require('../controllers/alumnosController');
const profesoresController = require('../controllers/profesoresController');
const empresasController = require('../controllers/empresasController');
const carrerasController = require('../controllers/carrerasController');
const gruposController = require('../controllers/gruposController');
const homeController = require('../controllers/homeController');
const materiasController = require('../controllers/materiasController');
const periodosController = require('../controllers/periodosController');
const cursosController = require('../controllers/cursosController');
const configuracionController = require('../controllers/configuracionController');

const conexion = require('../db/db');

/*----------------------------------------------------------------- */
//Vista de Dashboard 
router.get('/', accountController.isAuthenticated, homeController.conteoCuentas, homeController.conteoAlumnosCarrera, (req, res) => {
    // res.render('home/index',
    //     {
    //         usuario: req.usuario, admin: req.admin, alumno: req.alumno, profesor: req.profesor, empresa: req.empresa,
    //         sistemas: req.sistemas, industrial: req.industrial, mecatronica: req.mecatronica, electronica: req.electronica,
    //         empresarial: req.empresarial, contador: req.contador
    //     });
    res.render('home/index',
        {
            usuario: req.usuario, admin: req.admin, alumno: req.alumno, profesor: req.profesor, empresa: req.empresa,
            conteoAlumnosCarrera: req.conteoAlumnosCarrera
        });
});
router.get('/alumno/:id', accountController.isAuthenticated, alumnosController.infoAlumno, (req, res) => {
    res.render('home/alumno',
        {
            usuario: req.usuario, alumno: req.alumnoA
        });
});
router.get('/empresa/:id', accountController.isAuthenticated, empresasController.obtenerEmpresas, (req, res) =>
{
    res.render('home/empresa',
        {
            usuario: req.usuario, empresas: req.empresas
        });
});
router.post('/empresa/:id', empresasController.obtenerEmpresas, (req, res) =>
{
    res.render('home/empresa',
        {
            usuario: req.usuario, empresas: req.empresas
        });
});
router.get('/registrar_curso/:id', accountController.isAuthenticated, alumnosController.infoAlumno, configuracionController.obtenerConfiguracion, periodosController.obtenerPeriodos, (req, res) => {
    res.render('alumnos/registrar_curso',
        {
            usuario: req.usuario, alumno: req.alumnoA, configuracion: req.configuracion, periodos: req.periodos
        });
});
router.get('/cursos/:id', accountController.isAuthenticated, alumnosController.infoAlumno, configuracionController.obtenerConfiguracion, periodosController.obtenerPeriodos, (req, res) => {
    res.render('alumnos/mis_cursos',
        {
            usuario: req.usuario, 
            alumno: req.alumnoA, 
            configuracion: req.configuracion, 
            periodos: req.periodos
        });
});
/*----------------------------------------------------------------- */

/*----------------------------------------------------------------- */
//Vista del Login
router.get('/login', (req, res) => {
    res.render('account/login', { alert: false });
});
/*----------------------------------------------------------------- */

/*----------------------------------------------------------------- */
//Vista para notificaciones
router.get('/ventana', (req, res) => {
    res.render('_shared/ventana', { alert: false });
});
/*----------------------------------------------------------------- */

/*----------------------------------------------------------------- */
//Vista para configuración
router.get('/config', accountController.isAuthenticated, periodosController.obtenerPeriodos, (req, res) => {
    res.render('_shared/configuracion', { usuario: req.usuario, periodos: req.periodos });
});
router.get('/obtenerConfiguracion', configuracionController.obtenerConfiguracion, (req, res) => {
    res.json(req.configuracion);
});
router.post('/guardarConfiguracion', configuracionController.guardarConfiguracion);
/*----------------------------------------------------------------- */

/*----------------------------------------------------------------- */
//vista de profesores
router.get('/registrar_profesor', accountController.isAuthenticated, (req, res) => {
    res.render('profesores/registrar_profesor', { usuario: req.usuario });

});
router.get('/listar_profesores', accountController.isAuthenticated, profesoresController.listaProfesores, (req, res) => {
    res.render('profesores/listar_profesores', { usuario: req.usuario, profesores: req.profesores });
});
router.get('/actualizar_profesor', accountController.isAuthenticated, profesoresController.infoProfesor, (req, res) => {
    res.render('profesores/actualizar_profesor', { usuario: req.usuario, profesor: req.profesor });
});
router.get('/eliminar_profesor', accountController.isAuthenticated, profesoresController.eliminar_profesor);

router.get('/teacherProfile', accountController.isAuthenticated, profesoresController.infoProfesor, async (req, res) => {
    res.render('profesores/perfil', { usuario: req.usuario, profesor: req.profesor });
});
router.get('/teacherCourses', accountController.isAuthenticated, profesoresController.infoProfesor, periodosController.obtenerPeriodos, async (req, res) =>
{
    res.render('profesores/cursos', { usuario: req.usuario, profesor: req.profesor, periodos: req.periodos });
});
router.post('/obtenerCursosProfesor', cursosController.obtenerCursosProfesor);
/*----------------------------------------------------------------- */
router.get('/empresas', accountController.isAuthenticated, empresasController.obtenerEmpresas, (req, res) =>
{
    res.render('empresas/empresas', { usuario: req.usuario, empresas: req.empresas });
});
router.get('/registrar_empresa', accountController.isAuthenticated, (req, res) =>
{
    res.render('empresas/registrar_empresa', { usuario: req.usuario });
});
router.get('/actualizar_empresa', accountController.isAuthenticated, empresasController.obtenerEmpresa, (req, res) =>
{
    res.render('empresas/actualizar_empresa', { usuario: req.usuario, empresa: req.empresa });
});
/*----------------------------------------------------------------- */
//vista de alumno
router.get('/registrar_alumnos', accountController.isAuthenticated, (req, res) => {
    res.render('alumnos/registrar_alumnos', { usuario: req.usuario });
});
//vista para listar alumnos
router.get('/listar_alumnos', accountController.isAuthenticated, alumnosController.listaAlumnos, (req, res) => {
    res.render('alumnos/listar_alumnos', { usuario: req.usuario, alumnos: req.alumnos });

});
//ver informacion de alumno
router.get('/info_alumnos:id', accountController.isAuthenticated, alumnosController.infoAlumno, (req, res) => {
    res.render('alumnos/info_alumnos', { usuario: req.usuario, contra: req.contra, alumnoU: req.alumnoU, alumnoA: req.alumnoA })
});
//eliminar alumno
router.get('/eliminar_alumno:id', accountController.isAuthenticated, (req, res) => {
    const id_usuario = req.params.id;
    conexion.query('DELETE FROM usuarios WHERE id_usuario=?', [id_usuario], async (error, results) => {
        if (error) { console.log(error) }
        res.redirect('/listar_alumnos');
    })
});
/*----------------------------------------------------------------- */

/*----------------------------------------------------------------- */
//vista materias
router.get('/materias', accountController.isAuthenticated, materiasController.listaMaterias, (req, res) => {
    res.render('materias/materias', { usuario: req.usuario, materias: req.materias });
});
router.get('/api/materias', materiasController.listaMateriasPorCarrera, (req, res) =>
{
    res.json({ materias: req.materias });
});
router.get('/registrar_materia', accountController.isAuthenticated, carrerasController.obtenerCarreras, (req, res) => {
    res.render('materias/registrar_materia', { usuario: req.usuario, carreras: req.carreras });
});
router.get('/actualizar_materia', accountController.isAuthenticated, carrerasController.obtenerCarreras, materiasController.obtenerMateria, (req, res) => {
    res.render('materias/actualizar_materia', { usuario: req.usuario, carreras: req.carreras, materia: req.materia });
});
/*----------------------------------------------------------------- */
 
/*----------------------------------------------------------------- */
//vista carreras
router.get('/carreras', accountController.isAuthenticated, carrerasController.obtenerCarreras, (req, res) => {
    res.render('carreras/carreras', { usuario: req.usuario, carreras: req.carreras });
});
router.get('/api/carreras', carrerasController.obtenerCarreras, (req, res) => {
    res.json({ carreras: req.carreras})
})
//vista registrar carreras
router.get('/registrar_carrera', accountController.isAuthenticated, (req, res) => {
    res.render('carreras/registrar_carrera', { usuario: req.usuario });
});
router.get('/actualizar_carrera', accountController.isAuthenticated, carrerasController.obtenerCarrera, (req, res) => {
    res.render('carreras/actualizar_carrera', { usuario: req.usuario, carrera: req.carrera });
});
/*----------------------------------------------------------------- */

/*----------------------------------------------------------------- */
//listar administradores
router.get('/listar_administradores', accountController.isAuthenticated, administradoresController.listaAdministradores, (req, res) => {
    res.render('administradores/listar_administradores', { usuario: req.usuario, administradores: req.administradores });
});
//registrar administradores
router.get('/registrar_administradores', accountController.isAuthenticated, (req, res) => {
    res.render('administradores/registrar_administradores', { usuario: req.usuario })
});
//ver informacion de administrador
router.get('/info_administradores:id', accountController.isAuthenticated, administradoresController.infoAdmins, (req, res) => {
    res.render('administradores/info_administradores', { usuario: req.usuario, contra: req.contra, adminU: req.adminU, adminA: req.adminA })
});
router.get('/eliminar_administrador', administradoresController.eliminar_administrador);

router.get('/grupos', accountController.isAuthenticated, gruposController.listarGrupos, (req, res) => {
    res.render('grupos/grupos', { usuario: req.usuario, grupos: req.grupos });
});
router.get('/registrar_grupo', accountController.isAuthenticated, carrerasController.obtenerCarreras, (req, res) => {
    res.render('grupos/registrar_grupo', { usuario: req.usuario, carreras: req.carreras });
});
router.get('/actualizar_grupo', accountController.isAuthenticated, gruposController.obtenerGrupo, carrerasController.obtenerCarreras, (req, res) => {
    res.render('grupos/actualizar_grupo', { usuario: req.usuario, grupo: req.grupo, carreras: req.carreras });
});

router.get('/periodos', accountController.isAuthenticated, periodosController.obtenerPeriodos, (req, res) => {
    res.render('periodos/periodos', { usuario: req.usuario, periodos: req.periodos });
});
router.get('/registrar_periodo', accountController.isAuthenticated, (req, res) => {
    res.render('periodos/registrar_periodo', { usuario: req.usuario });
});
router.get('/actualizar_periodo', accountController.isAuthenticated, periodosController.obtenerPeriodo, (req, res) => {
    res.render('periodos/actualizar_periodo', { usuario: req.usuario, periodo: req.periodo });
});

router.get('/cursos', accountController.isAuthenticated, cursosController.obtenerCursos, carrerasController.obtenerCarreras, (req, res) => {
    res.render('cursos/cursos', { usuario: req.usuario, cursos: req.cursos, carreras: req.carreras });
});
router.get('/registrar_curso', accountController.isAuthenticated, materiasController.listaMateriasPorCarrera, gruposController.listarGruposPorCarrera, profesoresController.listaProfesores, periodosController.obtenerPeriodos, (req, res) => {
    res.render('cursos/registrar_curso', { usuario: req.usuario, materias: req.materias, grupos: req.grupos, profesores: req.profesores, periodos: req.periodos });
});
router.get('/actualizar_curso', accountController.isAuthenticated, cursosController.obtenerCurso, materiasController.listaMateriasPorCarrera, gruposController.listarGruposPorCarrera, profesoresController.listaProfesores, periodosController.obtenerPeriodos, (req, res) => {
    res.render('cursos/actualizar_curso', { usuario: req.usuario, curso: req.curso, carrera: req.query.c, materias: req.materias, grupos: req.grupos, profesores: req.profesores, periodos: req.periodos });
});
router.post('/obtenerCursosPeriodoCarrera', accountController.isAuthenticated, cursosController.obtenerCursosPeriodoCarrera, (req, res) =>
{
    res.json({ cursos: req.cursos });
}); 
router.post('/api/obtenerAlumnosCursoEmpresa', cursosController.obtenerAlumnosCursoEmpresa);
router.post('/obtenerMisCursos', cursosController.obtenerCursosAlumno);
router.post('/obtenerAlumnosCurso', cursosController.obtenerAlumnosCurso);
router.post('/actualizarCalificacionAlumno', cursosController.actualizarCalificacionAlumno);
router.post('/registrarAlumnoCurso', accountController.isAuthenticated, cursosController.registrarAlumnoCurso);

/*----------------------------------------------------------------- */
router.post('/registrar_administradores', profesoresController.registrar_profesores);
router.post('/actualizar_administradores', administradoresController.actualizar_administrador);

router.post('/registrar_empresa', empresasController.registrar_empresa);
router.post('/actualizar_empresa', empresasController.actualizar_empresa);
router.post('/eliminar_empresa', empresasController.eliminar_empresa);

router.post('/registrar_profesor', profesoresController.registrar_profesores);
router.post('/actualizar_profesor', profesoresController.actualizar_profesor);

router.post('/registrar_alumnos', alumnosController.registrar_alumnos,);
router.post('/actualizar_alumnos', alumnosController.actualizar_alumnos);

router.post('/registrar_materia', materiasController.registrar_materia)
router.post('/actualizar_materia', materiasController.actualizar_materia)
router.post('/eliminar_materia', materiasController.eliminar_materia)

router.post('/registrar_carrera', carrerasController.registrar_carrera);
router.post('/actualizar_carrera', carrerasController.actualizar_carrera);
router.post('/eliminar_carrera', carrerasController.eliminar_carrera);

router.post('/registrar_grupo', gruposController.registrar_grupo);
router.post('/eliminar_grupo', gruposController.eliminar_grupo);
router.post('/actualizar_grupo', gruposController.actualizar_grupo);

router.post('/registrar_periodo', periodosController.registrar_periodo);
router.post('/eliminar_periodo', periodosController.eliminar_periodo);
router.post('/actualizar_periodo', periodosController.actualizar_periodo);

router.post('/registrar_curso', cursosController.registrar_curso);
router.post('/eliminar_curso', cursosController.eliminar_curso);
router.post('/actualizar_curso', cursosController.actualizar_curso);

router.post('/login', accountController.login);
router.get('/logout', accountController.logout);
module.exports = router;