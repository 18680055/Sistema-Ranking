-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-02-2023 a las 17:57:15
-- Versión del servidor: 10.4.25-MariaDB
-- Versión de PHP: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ranking`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumnos`
--

CREATE TABLE `alumnos` (
  `num_alumno` int(11) NOT NULL,
  `id_usuario` varchar(13) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `ap_paterno` varchar(30) NOT NULL,
  `ap_materno` varchar(30) NOT NULL,
  `fecha_nac` date NOT NULL,
  `direccion` varchar(80) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `fecha_ingreso` date NOT NULL,
  `id_carrera` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `alumnos`
--

INSERT INTO `alumnos` (`num_alumno`, `id_usuario`, `nombre`, `ap_paterno`, `ap_materno`, `fecha_nac`, `direccion`, `correo`, `fecha_ingreso`, `id_carrera`) VALUES
(1, '18680077', 'David', 'Aquino', 'Mendez', '2022-11-01', 'Del apantle 37 col.paraiso', '18680077@cuautla.tecnm.mx', '2022-11-30', 1),
(5, '18680051', 'Pedro', 'Aquino', 'Sanchez', '2022-10-03', 'Av. del Ferrocarril', '18680051@cuautla.tecnm.mx', '2022-11-01', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carreras`
--

CREATE TABLE `carreras` (
  `id_carrera` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `coordinador` varchar(100) NOT NULL,
  `correo` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `carreras`
--

INSERT INTO `carreras` (`id_carrera`, `nombre`, `coordinador`, `correo`) VALUES
(1, 'Sistemas Computacionales', 'Luis Adrián García García', 'coord.sistemas@cuautla.tecnm.mx'),
(2, 'Industrial', 'Ma. del Rosario Mendieta Anzúrez', 'coord.industrial@cuautla.tecnm.mx'),
(3, 'Mecatronica', 'Marco Antonio Crsitóbal Álvarez', 'coord.mecatronica@cuautla.tecnm.mx'),
(4, 'Electronica', 'Marco Antonio Cristóbal Álvarez', 'coord.electronica@cuautla.tecnm.mx'),
(5, 'Empresarial', 'Miriam Guadalupe Rendón Valdepeña', 'coord.gestion@cuautla.tecnm.mx'),
(7, 'Contador Público', 'El contador xd', 'contador@contador.com'),
(9, 'Maestría en Ingeniería Administrativa', 'Coordinador X', 'coordx@x.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuraciones`
--

CREATE TABLE `configuraciones` (
  `id_configuracion` int(11) NOT NULL,
  `periodo` varchar(100) DEFAULT NULL,
  `registroDeMaterias` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `configuraciones`
--

INSERT INTO `configuraciones` (`id_configuracion`, `periodo`, `registroDeMaterias`) VALUES
(1, '1', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cursos`
--

CREATE TABLE `cursos` (
  `id_curso` int(11) NOT NULL,
  `materia` varchar(30) DEFAULT NULL,
  `grupo` int(11) NOT NULL,
  `profesor` int(11) DEFAULT NULL,
  `periodo` int(11) DEFAULT NULL,
  `estatus` tinyint(1) DEFAULT NULL,
  `eliminado` tinyint(1) DEFAULT NULL,
  `fechaelimino` date DEFAULT NULL,
  `motivoelimino` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cursos`
--

INSERT INTO `cursos` (`id_curso`, `materia`, `grupo`, `profesor`, `periodo`, `estatus`, `eliminado`, `fechaelimino`, `motivoelimino`) VALUES
(11, 'SC_ED_4', 2, 2, 1, 1, 0, '0000-00-00', 'null'),
(12, 'MIA_CALC1', 5, 1, 3, 1, 0, '0000-00-00', 'null'),
(13, 'II_CC', 4, 4, 4, 1, 0, '0000-00-00', 'null');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cursos_detalle`
--

CREATE TABLE `cursos_detalle` (
  `id_cursos_detalle` int(11) NOT NULL,
  `curso` int(11) DEFAULT NULL,
  `alumno` varchar(13) DEFAULT NULL,
  `calificacion` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cursos_detalle`
--

INSERT INTO `cursos_detalle` (`id_cursos_detalle`, `curso`, `alumno`, `calificacion`) VALUES
(4, 11, '18680051', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresas`
--

CREATE TABLE `empresas` (
  `num_empresa` int(11) NOT NULL,
  `id_usuario` varchar(13) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `telefono` varchar(15) DEFAULT NULL,
  `direccion` varchar(150) DEFAULT NULL,
  `tipo_sector` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `empresas`
--

INSERT INTO `empresas` (`num_empresa`, `id_usuario`, `nombre`, `email`, `telefono`, `direccion`, `tipo_sector`) VALUES
(5, 'XD', 'Empresa XD', 'c@c.com', '7353226501', 'direccion xd', 'Privado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupos`
--

CREATE TABLE `grupos` (
  `id_grupo` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `carrera` int(11) DEFAULT NULL,
  `semestre` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `grupos`
--

INSERT INTO `grupos` (`id_grupo`, `nombre`, `carrera`, `semestre`) VALUES
(2, 'Grupo 1 Sistemas', 1, 1),
(3, 'Grupo 2 Sistemas', 1, 1),
(4, 'Grupo 1 II', 2, 1),
(5, 'Grupo 1 MIA', 9, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materias`
--

CREATE TABLE `materias` (
  `id_materia` varchar(30) NOT NULL,
  `nombre_materia` varchar(50) NOT NULL,
  `id_carrera` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `materias`
--

INSERT INTO `materias` (`id_materia`, `nombre_materia`, `id_carrera`) VALUES
('II_CC', 'Control de la Calidad', 2),
('MIA_CALC1', 'Calculo Vectorial 1', 9),
('SC_ED_4', 'Estructura de Datos', 1),
('SC_ISW', 'Ingeniería de Software', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `periodos`
--

CREATE TABLE `periodos` (
  `id_periodo` int(11) NOT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `desde` varchar(12) DEFAULT NULL,
  `hasta` varchar(12) DEFAULT NULL,
  `estatus` tinyint(1) DEFAULT NULL,
  `eliminado` tinyint(1) DEFAULT NULL,
  `fechaelimino` date DEFAULT NULL,
  `motivoelimino` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `periodos`
--

INSERT INTO `periodos` (`id_periodo`, `descripcion`, `desde`, `hasta`, `estatus`, `eliminado`, `fechaelimino`, `motivoelimino`) VALUES
(1, 'ENE/JUL 2023', '2023-01-30', '2023-07-20', 1, 0, NULL, NULL),
(2, 'AGO/DIC 2023', '2023-08-25', '2023-12-17', 1, 0, NULL, NULL),
(3, 'ENE/JUL 2024', '2024-02-23', '2024-08-23', 1, 0, NULL, NULL),
(4, 'AGO/DIC 2024	', '2024-08-17', '2024-12-17', 1, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profesores`
--

CREATE TABLE `profesores` (
  `num_profesor` int(11) NOT NULL,
  `id_usuario` varchar(13) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `ap_paterno` varchar(30) NOT NULL,
  `ap_materno` varchar(30) NOT NULL,
  `cedula` varchar(10) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `tipo_contrato` varchar(45) NOT NULL,
  `grado_academico` varchar(45) NOT NULL,
  `tipo_academia` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `profesores`
--

INSERT INTO `profesores` (`num_profesor`, `id_usuario`, `nombre`, `ap_paterno`, `ap_materno`, `cedula`, `correo`, `tipo_contrato`, `grado_academico`, `tipo_academia`) VALUES
(1, 'A2C3591011', 'Edgar', 'Garcia', 'Muñoz', '9999999999', 'A2C3591011@cuautla.tecnm.mx', 'Honorario', 'Ing.', 'Basicas'),
(2, '18680055', 'Julio Edgar', 'Beltran', 'Garcia', '123456789', '18680055@cuautla.tecnm.mx', 'Honorario', 'Ing. Sistemas Computacionales', 'Basicas'),
(3, 'A2C3591012', 'Edgar', 'Beltran', 'Garcia', '123457890', 'A2C3591012@cuautla.tecnm.mx', 'Base', 'Ingeniero Industrial', 'Especialidad'),
(4, '18680290', 'ProfesorXD', 'xd', 'xd', '1', 'xd@xd.com', 'Honorario', 'xd', 'Especialidad');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `idrol` int(11) NOT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` varchar(13) NOT NULL,
  `contra` varchar(100) NOT NULL,
  `rol` varchar(13) NOT NULL,
  `estatus` varchar(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `contra`, `rol`, `estatus`) VALUES
('18680051', 'U2FsdGVkX1/2vehOzx8FNzOxCYx6p2DfrPHIBd3YOSg=', 'Alumno', 'Activo'),
('18680055', 'U2FsdGVkX18SUAtplMi35yh+zftnp3RN09ZUTwWidBw=\r\n', 'Administrador', 'Activo'),
('18680077', 'U2FsdGVkX1+VOF5N+j6pdUza/WJuY79LakDN6Zko1H8=', 'Alumno', 'Activo'),
('18680290', 'U2FsdGVkX19IhtaD+O5CokG2SLWq/6HCwUon5s4nO7A=', 'Profesor', 'Activo'),
('A2C3591011', 'U2FsdGVkX1+kB8+ioSnkCFYAip4p53QG34JQq179YvI=', 'Profesor', 'Activo'),
('A2C3591012', 'U2FsdGVkX18SUAtplMi35yh+zftnp3RN09ZUTwWidBw=\r\n', 'Administrador', 'Activo'),
('XD', 'U2FsdGVkX1+QhpRtAzk3SNQqyKVi/oDlnqOOFFsXwGI=', 'Empresa', 'Activo');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `alumnos`
--
ALTER TABLE `alumnos`
  ADD PRIMARY KEY (`num_alumno`),
  ADD KEY `id_carrera` (`id_carrera`),
  ADD KEY `alumnos_ibfk_1` (`id_usuario`);

--
-- Indices de la tabla `carreras`
--
ALTER TABLE `carreras`
  ADD PRIMARY KEY (`id_carrera`);

--
-- Indices de la tabla `configuraciones`
--
ALTER TABLE `configuraciones`
  ADD PRIMARY KEY (`id_configuracion`);

--
-- Indices de la tabla `cursos`
--
ALTER TABLE `cursos`
  ADD PRIMARY KEY (`id_curso`),
  ADD KEY `materia` (`materia`),
  ADD KEY `profesor` (`profesor`),
  ADD KEY `periodo` (`periodo`),
  ADD KEY `grupo` (`grupo`);

--
-- Indices de la tabla `cursos_detalle`
--
ALTER TABLE `cursos_detalle`
  ADD PRIMARY KEY (`id_cursos_detalle`),
  ADD KEY `curso` (`curso`),
  ADD KEY `alumno` (`alumno`);

--
-- Indices de la tabla `empresas`
--
ALTER TABLE `empresas`
  ADD PRIMARY KEY (`num_empresa`),
  ADD KEY `empresas_ibfk_1` (`id_usuario`);

--
-- Indices de la tabla `grupos`
--
ALTER TABLE `grupos`
  ADD PRIMARY KEY (`id_grupo`),
  ADD KEY `carrera` (`carrera`);

--
-- Indices de la tabla `materias`
--
ALTER TABLE `materias`
  ADD PRIMARY KEY (`id_materia`),
  ADD KEY `id_carrera` (`id_carrera`);

--
-- Indices de la tabla `periodos`
--
ALTER TABLE `periodos`
  ADD PRIMARY KEY (`id_periodo`);

--
-- Indices de la tabla `profesores`
--
ALTER TABLE `profesores`
  ADD PRIMARY KEY (`num_profesor`),
  ADD KEY `profesores_ibfk_1` (`id_usuario`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`idrol`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `alumnos`
--
ALTER TABLE `alumnos`
  MODIFY `num_alumno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `carreras`
--
ALTER TABLE `carreras`
  MODIFY `id_carrera` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `configuraciones`
--
ALTER TABLE `configuraciones`
  MODIFY `id_configuracion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `cursos`
--
ALTER TABLE `cursos`
  MODIFY `id_curso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `cursos_detalle`
--
ALTER TABLE `cursos_detalle`
  MODIFY `id_cursos_detalle` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `empresas`
--
ALTER TABLE `empresas`
  MODIFY `num_empresa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `grupos`
--
ALTER TABLE `grupos`
  MODIFY `id_grupo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `periodos`
--
ALTER TABLE `periodos`
  MODIFY `id_periodo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `profesores`
--
ALTER TABLE `profesores`
  MODIFY `num_profesor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `idrol` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `alumnos`
--
ALTER TABLE `alumnos`
  ADD CONSTRAINT `alumnos_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `alumnos_ibfk_2` FOREIGN KEY (`id_carrera`) REFERENCES `carreras` (`id_carrera`);

--
-- Filtros para la tabla `cursos`
--
ALTER TABLE `cursos`
  ADD CONSTRAINT `cursos_ibfk_1` FOREIGN KEY (`materia`) REFERENCES `materias` (`id_materia`),
  ADD CONSTRAINT `cursos_ibfk_2` FOREIGN KEY (`profesor`) REFERENCES `profesores` (`num_profesor`),
  ADD CONSTRAINT `cursos_ibfk_3` FOREIGN KEY (`periodo`) REFERENCES `periodos` (`id_periodo`),
  ADD CONSTRAINT `cursos_ibfk_4` FOREIGN KEY (`grupo`) REFERENCES `grupos` (`id_grupo`);

--
-- Filtros para la tabla `cursos_detalle`
--
ALTER TABLE `cursos_detalle`
  ADD CONSTRAINT `cursos_detalle_ibfk_1` FOREIGN KEY (`curso`) REFERENCES `cursos` (`id_curso`),
  ADD CONSTRAINT `cursos_detalle_ibfk_2` FOREIGN KEY (`alumno`) REFERENCES `usuarios` (`id_usuario`);

--
-- Filtros para la tabla `empresas`
--
ALTER TABLE `empresas`
  ADD CONSTRAINT `empresas_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `grupos`
--
ALTER TABLE `grupos`
  ADD CONSTRAINT `grupos_ibfk_1` FOREIGN KEY (`carrera`) REFERENCES `carreras` (`id_carrera`);

--
-- Filtros para la tabla `materias`
--
ALTER TABLE `materias`
  ADD CONSTRAINT `materias_ibfk_1` FOREIGN KEY (`id_carrera`) REFERENCES `carreras` (`id_carrera`);

--
-- Filtros para la tabla `profesores`
--
ALTER TABLE `profesores`
  ADD CONSTRAINT `profesores_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
